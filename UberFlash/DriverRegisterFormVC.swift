//
//  DriverRegisterFormVC.swift
//  UberFlash
//
//  Created by Chính Trình Quang on 12/12/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import Firebase
class DriverRegisterFormVC:UIViewController{
    @IBOutlet weak var fullname: UITextField!
    @IBOutlet weak var id: UITextField!
    @IBOutlet weak var Gender: UITextField!
    @IBOutlet weak var birthday: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBAction func Enroll(_ sender: Any) {
        if fullname.text?.count == 0 || id.text?.count == 0 {
            let alert = UIAlertController(title: "Error", message: "Name or ID field cannot be null", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                
            })
            present(alert,animated: true)
        }
        else{
            let bundle = ["fullname" : "\(fullname.text ?? "")",
                "ID" : "\(id.text ?? "")",
                "gender": "\(Gender.text ?? "")",
                "date_of_birth": "\(birthday.text ?? "")",
                "address" : "\(address.text ?? "")",
                "distance" : "0"
            ]
            let ref = FIRDatabase.database().reference()
           
            
            
            ref.child("drivers").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.hasChild(GoogleSignInController.ID){
                    self.isExist()
                    
                    
                }else{
                    ref.child("drivers").child(GoogleSignInController.ID).setValue(bundle)
                    self.successful_signup_driver_account()
                    
                }
                
                
            })
        }
        
        
        
    }
    func isExist()  {
        
        let alert = UIAlertController(title: "Notice", message: "This account has already signed up as driver", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            
        })
        present(alert,animated: true)
    }
    func successful_signup_driver_account(){
        let alert = UIAlertController(title: "Sign up successfully for driver account !", message: "Go back to the menu", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
           
            self.navigationController?.popViewController(animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                alert.dismiss(animated: true, completion: nil)
            })
            
            
        })
        present(alert,animated: true)
    }
   
    func segue(){
        performSegue(withIdentifier: "RegisterDoneSegue", sender: nil)
    }
    override func viewDidLoad() {
        super .viewDidLoad()
        UI.addDoneButton(controls: [fullname,id,Gender,birthday,address])
        registerKeyboardNotification();
    }
    override func viewDidDisappear(_ animated: Bool) {
        super .viewDidDisappear(animated)
        deregidterKeyboardNotification()
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        scrollview.isScrollEnabled = true
        let info = notification.userInfo;
        let keyboard = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInset = UIEdgeInsets(top: 0.0,left: 0.0,bottom: keyboard.height,right: 0.0);
        self.scrollview.contentInset = contentInset;
        self.scrollview.scrollIndicatorInsets = contentInset;
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        
        let info = notification.userInfo;
        let keyboard = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInset = UIEdgeInsets(top: 0.0,left: 0.0,bottom: -keyboard.height,right: 0.0);
        self.scrollview.contentInset = contentInset;
        self.scrollview.scrollIndicatorInsets = contentInset;
    }
    func registerKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func deregidterKeyboardNotification(){
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification,object : nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification,object : nil)
    }

}
