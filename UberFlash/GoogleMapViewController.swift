import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import MapKit
import Alamofire
import Stripe
import Firebase
import FirebaseAuth
import FirebaseDatabase
class GoogleMapViewController: UIViewController, UISearchBarDelegate, LocateOnTheMap, GMSAutocompleteFetcherDelegate,CLLocationManagerDelegate ,GMSMapViewDelegate,STPAddCardViewControllerDelegate{
    // Variable
    var locationManager: CLLocationManager!
    static var firstload : Int = 0
    var GoogleMapsView: GMSMapView!
    var searchResultController: SearchResultsController!
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    var markerCurrent : GMSMarker = GMSMarker()
    var markerDest : GMSMarker = GMSMarker()
    var polyline : GMSPolyline = GMSPolyline()
    var PointCount = 0
    var distanceTotal : Double = 0.0
    var startAddr : String = String()
    var endAddr : String = String()
    static var throughCast = 1
    static var AutoID : String = String()
    static var isCalling = 0
    static var token : String = String()
    //Outlet inflater
    @IBOutlet weak var btnCallDriver: UIButton!
    @IBOutlet weak var txtDistance: UITextField!
    @IBOutlet weak var txtDuration: UITextField!
    @IBAction func SearchAddress(_ sender: Any) {
        let searchController = UISearchController(searchResultsController: searchResultController)
        searchController.searchBar.delegate = self
        UI.addDoneButton(controls: [searchController.searchBar])
        self.present(searchController, animated: true, completion: nil)
    }
    
    @IBAction func showMeTheWay(_ sender: Any) {
        self.draw(src: self.markerCurrent.position, dst: self.markerDest.position)
    }
    @IBOutlet weak var GoogleMapsContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        dismiss(animated: true)
        GoogleMapViewController.firstload = 0
        GoogleMapViewController.isCalling = 0
    }
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        dismiss(animated: true)
        
        GoogleMapViewController.token = token.tokenId
        print(GoogleMapViewController.token)
        let url :String = "https://transactiontesting.herokuapp.com/charge"
        // 2
        let amount = (Double)(txtDistance.text!)! * 10 * 100
        let params: [String: Any] = [
            "token": token.tokenId,
            "amount": amount,
            "currency": "usd",
            "description": "UberFlash"
        ]
        // 3
        var code = 0;
        Alamofire.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                let json = response.result.value as! [String: Any]
                code = json["code"] as! Int
                print(code)
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    self.TransactionNotice(code: code)
                    GoogleMapViewController.firstload = 0
                })

                
        }
       
        
    }
    func TransactionNotice(code : Int){
        if code == 1{
            
            let alert = UIAlertController(title: "Add visa card successfully", message: nil, preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                GoogleMapViewController.isCalling = 1
                self.pushUserInfo()
                
                GoogleMapViewController.firstload = 0
                
            })
            
            present(alert, animated: true)
        }else {
            let alert = UIAlertController(title: "Add visa card unsuccessfully", message: nil, preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                GoogleMapViewController.isCalling = 0
                GoogleMapViewController.firstload = 0
                
            })
            
            present(alert, animated: true)
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.GoogleMapsView = GMSMapView(frame: self.GoogleMapsContainer.frame)
        self.view.addSubview(self.GoogleMapsView)
        GoogleMapsView.delegate = self
        if GoogleMapViewController.isCalling == 1
        {
            self.btnCallDriver.setTitle("Cancel the drive", for: .normal)
        }else {
            self.btnCallDriver.setTitle("Call driver (10$/1km)", for: .normal)
        }
        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        PointCount = 1
        
        GoogleMapViewController.firstload = 0
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        markerCurrent.map = nil
        let location = locations.last! as CLLocation
        if GoogleMapViewController.isCalling == 1 {
            if GoogleMapViewController.throughCast == 1{
                let ref: FIRDatabaseReference! = FIRDatabase.database().reference()
                
                ref.child("customers").child(GoogleMapViewController.AutoID).child("lat_Current").setValue("\(location.coordinate.latitude)")
                ref.child("customers").child(GoogleMapViewController.AutoID).child("lon_Current").setValue("\(location.coordinate.longitude)")
            }
            
        }
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        //let position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        markerCurrent = GMSMarker(position: center)
        if GoogleMapViewController.firstload == 0{
            
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 16)
            self.GoogleMapsView.camera = camera
            GoogleMapViewController.firstload = GoogleMapViewController.firstload + 1
        }
        
        
        
        markerCurrent.icon = UIImage(named: "marker_style5.png")
        
        markerCurrent.map = self.GoogleMapsView
        
            
        
    }
    public func didFailAutocompleteWithError(_ error: Error) {
        //        resultText?.text = error.localizedDescription
    }
    
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        
        
        for prediction in predictions {
            
            if let prediction = prediction as GMSAutocompletePrediction!{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        self.searchResultController.reloadDataWithArray(self.resultsArray)
    }
    
    func locateWithLongitude(lon: Double, andLatitude lat: Double, andTitle title: String) {
        
        DispatchQueue.main.async { () -> Void in
            self.markerDest.map = nil
            let position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            self.markerDest = GMSMarker(position: position)
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 16)
            
            self.GoogleMapsView.camera = camera
            
            self.markerDest.title = "Address: \(title)"
            self.markerDest.map = self.GoogleMapsView
            //self.draw(src: self.markerCurrent.position, dst: self.markerDest.position)
            self.updateLocation(src: self.markerCurrent.position, dst: self.markerDest.position)
        }
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        PointCount = 2
        DispatchQueue.main.async { () -> Void in
            self.markerDest.map = nil
            
            
            
            self.markerDest = GMSMarker(position: coordinate)
            self.markerDest.title = "New Marker"
            
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 16)
            
            self.GoogleMapsView.camera = camera
            self.markerDest.map = self.GoogleMapsView
            self.polyline.map = nil
            //self.draw(src: self.markerCurrent.position, dst: self.markerDest.position)
            self.updateLocation(src: self.markerCurrent.position, dst: self.markerDest.position)
        }
    }

    func updateLocation (src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        PointCount = 2
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&mode=driving&key=AIzaSyCOPRYcMvUCduB1IP-yuy6oXVokQZpMjl0")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                print(String(data: data!, encoding: .utf8)!)
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        //-----------------------------------------------
                        let preRoutes = json["routes"] as! NSArray
                        let routes = preRoutes[0] as! NSDictionary
                        let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                        let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                        
                        //-----------------------------------------------
                        let legs = routes["legs"] as! NSArray
                        let legsParse = legs[0] as! NSDictionary
                        
                        
                        //----------steps to take to dest point----------
                        let steps : NSArray = legsParse.value(forKey: "steps") as! NSArray
                        var i = 0
                        var distanceSum = 0
                        var durationSum = 0
                        while (i < steps.count){
                            let subStep : NSDictionary = steps[i] as! NSDictionary
                            //get distance
                            let distance :NSDictionary = subStep["distance"] as! NSDictionary
                            let dicstanceVal : Int = distance["value"] as! Int
                            distanceSum = distanceSum + dicstanceVal
                            
                            //get duration
                            let duration :NSDictionary = subStep["duration"] as! NSDictionary
                            let durationVal : Int = duration["value"] as! Int
                            durationSum = durationSum + durationVal
                            i = i + 1
                        }
                        
                        print ("Quãng đường phải đi là : \(distanceSum)")
                        print ("Thời gian tổng cộng là : \(durationSum)")
                        //let distanceSumEst : Double = Double(distanceSum) / 1000
                        //let durationSumEst :Double = Double (durationSum) / 60
                        let x = Double(Double(distanceSum) / 1000).rounded(toPlaces: 1)
                        let y = Double(Double(durationSum) / 1000).rounded(toPlaces: 1)
                        
                        // Save distance value as global static variable
                        self.distanceTotal = x
                        
                        
                        // ---------get dest address and current address-------
                        let end_address : String = legsParse["end_address"] as! String
                        let start_address : String = legsParse["start_address"] as! String
                        self.endAddr = end_address
                        self.startAddr = start_address
                        
                        //                        print(start_address)
                        //                        print(end_address)
                        
                        
                        DispatchQueue.main.async(execute: {
//                            self.polyline.map = nil
//                            let path = GMSPath(fromEncodedPath: polyString)
//                            self.polyline = GMSPolyline(path: path)
//                            self.polyline.strokeWidth = 5.0
//                            self.polyline.strokeColor = UIColor.blue
//                            self.polyline.map = self.GoogleMapsView
                            self.txtDistance.text = "\(x)"
                            self.txtDuration.text = "\(y)"
                        })
                    }
                    
                } catch {
                    print("parsing error")
                }
            }
        })
        task.resume()
    }
    func draw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        PointCount = 2
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&mode=driving&key=AIzaSyCOPRYcMvUCduB1IP-yuy6oXVokQZpMjl0")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                print(String(data: data!, encoding: .utf8)!)
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        //-----------------------------------------------
                        let preRoutes = json["routes"] as! NSArray
                        let routes = preRoutes[0] as! NSDictionary
                        let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                        let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                        
                        //-----------------------------------------------
                        let legs = routes["legs"] as! NSArray
                        let legsParse = legs[0] as! NSDictionary
                        
                        
                        //----------steps to take to dest point----------
                        let steps : NSArray = legsParse.value(forKey: "steps") as! NSArray
                        var i = 0
                        var distanceSum = 0
                        var durationSum = 0
                        while (i < steps.count){
                            let subStep : NSDictionary = steps[i] as! NSDictionary
                            //get distance
                            let distance :NSDictionary = subStep["distance"] as! NSDictionary
                            let dicstanceVal : Int = distance["value"] as! Int
                            distanceSum = distanceSum + dicstanceVal
                            
                            //get duration
                            let duration :NSDictionary = subStep["duration"] as! NSDictionary
                            let durationVal : Int = duration["value"] as! Int
                            durationSum = durationSum + durationVal
                            i = i + 1
                        }
                        
                        print ("Quãng đường phải đi là : \(distanceSum)")
                        print ("Thời gian tổng cộng là : \(durationSum)")
                        //let distanceSumEst : Double = Double(distanceSum) / 1000
                        //let durationSumEst :Double = Double (durationSum) / 60
                        let x = Double(Double(distanceSum) / 1000).rounded(toPlaces: 1)
                        let y = Double(Double(durationSum) / 1000).rounded(toPlaces: 1)
                        
                        // Save distance value as global static variable
                        self.distanceTotal = x
                        
                        
                        // ---------get dest address and current address-------
                        let end_address : String = legsParse["end_address"] as! String
                        let start_address : String = legsParse["start_address"] as! String
                        self.endAddr = end_address
                        self.startAddr = start_address
                        
//                        print(start_address)
//                        print(end_address)
                        
                        
                        DispatchQueue.main.async(execute: {
                            self.polyline.map = nil
                            let path = GMSPath(fromEncodedPath: polyString)
                            self.polyline = GMSPolyline(path: path)
                            self.polyline.strokeWidth = 5.0
                            self.polyline.strokeColor = UIColor.blue
                            self.polyline.map = self.GoogleMapsView
                            self.txtDistance.text = "\(x)"
                            self.txtDuration.text = "\(y)"
                        })
                    }
                    
                } catch {
                    print("parsing error")
                }
            }
        })
        task.resume()
    }
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = self.GoogleMapsView // Your map view
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.resultsArray.removeAll()
        gmsFetcher?.sourceTextHasChanged(searchText)
    }
    
    @IBAction func callDriver(_ sender: Any) {
        
        if GoogleMapViewController.isCalling == 0 {
            
            if PointCount != 2{
                let alert = UIAlertController(title: "Please choose a place you want to go", message: nil, preferredStyle: .alert)
                
                
                alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                    
                })
                present(alert, animated: true)
                
            }else{
                let alert = UIAlertController(title: "Choose payment methods", message: nil, preferredStyle: .alert)
                
                
                alert.addAction(UIAlertAction(title: "Through cast", style: .default) { _ in
                    GoogleMapViewController.isCalling = 1
                    GoogleMapViewController.throughCast = 1
                    self.btnCallDriver.setTitle("Cancel the drive", for: .normal)
                    self.CastPay()
                })
                
                alert.addAction(UIAlertAction(title: "Through visa", style: .default) { _ in
                    GoogleMapViewController.throughCast = 0
                    GoogleMapViewController.isCalling = 1
                    self.VisaPay()
                    
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: .destructive) { _ in
                    GoogleMapViewController.isCalling = 0
                })
                present(alert, animated: true)
            }
            
        }else {
            let alert = UIAlertController(title: "Do you want to cancel the drive", message: nil, preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default) { _ in
                GoogleMapViewController.isCalling = 0
                self.btnCallDriver.setTitle("Call driver (10$/1km)", for: .normal)
                self.CancelDrive()
            })
            
            alert.addAction(UIAlertAction(title: "No", style: .default) { _ in
                
                
            })
            present(alert, animated: true)
        }
            
        
        
    }
    func CancelDrive(){
        let ref: FIRDatabaseReference! = FIRDatabase.database().reference()
        ref.child("customers").child(GoogleMapViewController.AutoID).removeValue()
        print(GoogleMapViewController.AutoID)
        
        
    }
    func VisaPay(){
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        
        // Present add card view controller
        let navigationController = UINavigationController(rootViewController: addCardViewController)
        
        present(navigationController, animated: true)
    }
    func CastPay(){
        pushUserInfo()
    }
    func pushUserInfo() {
        let userinfo = CustomerInfo()
        //username: GoogleSignInController.fullname, lat_Current: markerCurrent.position.latitude, lon_Current: markerCurrent.position.longitude, lat_Dest: markerDest.position.latitude, lon_Dest: markerDest.position.longitude, distance: distanceTotal, ID: GoogleSignInController.ID
        userinfo.ID = GoogleSignInController.ID
        userinfo.current_Address = startAddr
        userinfo.username = GoogleSignInController.fullname
        userinfo.distance = distanceTotal
        userinfo.dest_Address = endAddr
        let is_occupied = 0;
        let bundle = ["username" : "\(userinfo.username)",
            "distance" : "\(userinfo.distance)",
            "uid": "\(userinfo.ID)",
            "start_address" : "\(userinfo.current_Address)",
            "end_address" : "\(userinfo.dest_Address)",
            "is_occupied" : "\(is_occupied)",
            "lat_Current": "\(markerCurrent.position.latitude)",
            "lon_Current": "\(markerCurrent.position.longitude)",
            "lat_Dest": "\(markerDest.position.latitude)",
            "lon_Dest": "\(markerDest.position.longitude)",
            "payment_method" : "\(GoogleMapViewController.throughCast)"
        ]
        let ref: FIRDatabaseReference! = FIRDatabase.database().reference()
        //ref.child("customers").childByAutoId().setValue(bundle)
        GoogleMapViewController.AutoID = ref.child("customers").childByAutoId().key
        ref.child("customers").child(GoogleMapViewController.AutoID).setValue(bundle)
        GoogleMapViewController.throughCast = 1
        
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
