//
//  Customer.swift
//  UberFlash
//
//  Created by Chính Trình Quang on 11/30/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import MapKit
import Alamofire
import Stripe
class CustomerInfo {
    var username : String = String();
    var lat_Current : Double = Double();
    var lon_Current : Double = Double();
    var current_Address : String = String()
    var dest_Address :String = String()
    var lat_Dest : Double = Double();
    var lon_Dest : Double = Double();
    var distance : Double = Double();
    var ID : String = String();
    var payment_method : Int = Int()
    init(username : String,current_Address : String,dest_Address :String, lat_Current : Double,lon_Current : Double,lat_Dest :Double,lon_Dest : Double, distance : Double, ID : String, payment_method : Int){
        self.username = username
        self.lat_Current = lat_Current
        self.lon_Current = lon_Current
        self.lat_Dest = lat_Dest
        self.lon_Dest = lon_Dest
        self.distance = distance
        self.current_Address = current_Address
        self.dest_Address = dest_Address
        self.ID = ID
        self.payment_method = payment_method;
    }
    init() {
        self.username = ""
        self.lat_Current = 0
        self.lon_Current = 0
        self.lat_Dest = 0
        self.lon_Dest = 0
        self.distance = 0
        self.current_Address = ""
        self.dest_Address = ""
        self.ID = ""
        self.payment_method = 1;
    }
}
