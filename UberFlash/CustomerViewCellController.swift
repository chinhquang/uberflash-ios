//
//  CustomerViewCellController.swift
//  UberFlash
//
//  Created by Chính Trình Quang on 12/1/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
class CustomerViewCellController : UITableViewCell{
    
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var destinationLocation: UILabel!
    @IBOutlet weak var currentLocation: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var payment_method: UILabel!
    func setCell (user : CustomerInfo){
        username.text = user.username
        destinationLocation.text = user.dest_Address
        currentLocation.text = user.current_Address
        distance.text = String(user.distance) + " km"
        payment_method.text = (user.payment_method == 1) ? "Through cast" : "Through visa"
    }
}
