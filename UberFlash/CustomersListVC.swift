//
//  CustomersList.swift
//  UberFlash
//
//  Created by Chính Trình Quang on 12/1/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import Firebase
class CustomersListVC:UIViewController,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = List[indexPath.row]
        let cell =  table.dequeueReusableCell(withIdentifier: "userinfo") as! CustomerViewCellController
        
        
        cell.setCell(user: item);
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.gray
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Are you sure to handle this drive", message: nil, preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { _ in
            let ref: FIRDatabaseReference! = FIRDatabase.database().reference()
            ref.child("customers").child(self.IDList[indexPath.row]).child("is_occupied").setValue("1")
            var x = 0.0
            let y = self.List[indexPath.row].payment_method
            let z = self.IDList[indexPath.row]
            let i = self.List[indexPath.row].distance
            ref.child("drivers").child(GoogleSignInController.ID).child("distance").observeSingleEvent(of: .value) {
                (snapshot) in
                x = Double(snapshot.value as! String)!
                if y == 0
                {
                    x = x + i
                }
                ref.child("drivers").child(GoogleSignInController.ID).child("IDChuyenDi").setValue("\(z)")
                ref.child("drivers").child(GoogleSignInController.ID).child("distance").setValue("\(x)")
                self.segue()
            }
            
            
            
            
           
            
            
            
            
        })
        alert.addAction(UIAlertAction(title: "No", style: .default) { _ in
            
        })
        present(alert, animated: true)
    }
    func segue(){
        
        performSegue(withIdentifier: "ShowPlaceSegue", sender: nil)
    }
    
    var List : [CustomerInfo] = [CustomerInfo]()
    var IDList : [String] = [String]()
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table.backgroundColor = UIColor.lightGray
        table.delegate = self
        table.dataSource = self
        let ref = FIRDatabase.database().reference()
        
        let customers  = ref.child("customers")
        customers.observe(.value, with: {(snapshot) in
            self.List.removeAll()
            self.IDList.removeAll()
            if snapshot.childrenCount > 0 {
                
                
                for users in snapshot.children.allObjects as![FIRDataSnapshot]{
                    // lấy giá trị
                    let id =  users.key as String
                    
                    
                    let userObject = users.value as? [String: AnyObject]
                    let userName = userObject?["username"] as! String
                    let start_address = userObject?["start_address"] as! String
                    let end_address = userObject?["end_address"] as! String
                    let distance = Double(userObject?["distance"]as! String)
                    let ID = userObject?["uid"]as! String
                    
                    let lat_Current = Double(userObject?["lat_Current"]as! String)
                    let lon_Current = Double(userObject?["lon_Current"]as! String)
                    let lat_Dest = Double(userObject?["lat_Dest"]as! String)
                    let lon_Dest = Double(userObject?["lon_Dest"]as! String)
                    let payment_method = Int(userObject?["payment_method"] as! String)
                    let is_occupied = Int(userObject?["is_occupied"] as! String)
                    let user = CustomerInfo();
                    user.username = userName
                    user.dest_Address = end_address
                    user.current_Address = start_address
                    user.distance = distance!
                    user.ID = ID
                    user.lat_Current = lat_Current!
                    user.lat_Dest  = lat_Dest!
                    user.lon_Dest = lon_Dest!
                    user.lon_Current = lon_Current!
                    user.payment_method = payment_method!
                    // thêm user vừa tạo vào trong userList
                    if is_occupied == 0 {
                        self.List.append(user)
                        self.IDList.append(id)
                    }
                    
                    
                }
                
                self.table.reloadData()
            }else{
                self.table.reloadData()
            }
        })
        
    }
}
