//
//  SelectionController.swift
//  UberFlash
//
//  Created by Chính Trình Quang on 11/22/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import Firebase
class SelectionController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signOutSegue"{
            GoogleSignInController.isSignOut = 1
        }
        
    }
    @IBAction func gotolist(_ sender: Any) {
        let ref = FIRDatabase.database().reference()
        let drivers  = ref.child("drivers")
        
        ref.child("drivers").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChild(GoogleSignInController.ID){
                self.segue()
                
                
            }else{
                self.isNotSignedUp()
                
            }
            
            
        })
        
        
    }
    func isNotSignedUp() {
        let alert = UIAlertController(title: "Notice", message: "This account has not  signed up as a driver yet !", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            alert.dismiss(animated: true, completion: nil)
        })
        present(alert,animated: true)
    }
    
    func segue() {
        performSegue(withIdentifier: "goToListSegue", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = GoogleSignInController.fullname
        self.navigationItem.setHidesBackButton(true, animated:true);
        
    }
    
}
