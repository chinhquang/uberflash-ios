//
//  ViewController.swift
//  UberFlash
//
//  Created by Chính Trình Quang on 11/22/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import Google

class GoogleSignInController: UIViewController, UISearchBarDelegate, GIDSignInUIDelegate,GIDSignInDelegate {

    //SIGNIN
    static var fullname = String()
    static var ID = String()
    static var isSignOut : Int = 0
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        
        print("Google Sign In didSignInForUser")
        if let error = error {
            print(error.localizedDescription)
            return
        }else{
            GoogleSignInController.fullname = user.profile.name
            GoogleSignInController.ID = user.userID
            performSegue(withIdentifier: "SelectionSegue", sender: nil)
            //self.present(SelectionController, animated: true, completion: nil)
        }
        
        
    }
    
    //PRESENT
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        if let aController = viewController {
            present(aController, animated: true) {() -> Void in }
        }
    }
    
    //DISMISS
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        dismiss(animated: true) {() -> Void in }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        if(GoogleSignInController.isSignOut == 1){
            GIDSignIn.sharedInstance()?.signOut()
            do {
                try FIRAuth.auth()!.signOut()
                //self.dismiss(animated: true, completion: nil)
            } catch let err {
                print(err)
            }
            GoogleSignInController.isSignOut = 0;
        }
        
        if  FIRAuth.auth()?.currentUser != nil {
            GoogleSignInController.fullname = FIRAuth.auth()?.currentUser?.displayName ?? "Default"
            GoogleSignInController.ID = FIRAuth.auth()?.currentUser?.uid ?? "111"
            performSegue(withIdentifier: "SelectionSegue", sender: nil)
        } else {
            GIDSignIn.sharedInstance()?.clientID = "471457532537-a1s331efrpla93iv8ghd03gon29udej0.apps.googleusercontent.com"
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().uiDelegate = self
            
            
            var error : NSError?
            GGLContext.sharedInstance()?.configureWithError(&error)
            if error != nil {
                print(error as Any)
                
            }
            let signInButton = GIDSignInButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50));
            signInButton.center = view.center
            view.addSubview(signInButton)
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
        
    }

}

